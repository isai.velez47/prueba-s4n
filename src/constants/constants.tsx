export enum ACTION_TYPES {
  UPDATE_USER = 'UPDATE_USER',
}

export const MAX_COUNT_WITH_FILTER: number = 5;