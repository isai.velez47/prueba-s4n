import { ACTION_TYPES } from "../../constants/constants";

export const updateUser = user => ({
    type: ACTION_TYPES.UPDATE_USER,
    user,
})