import { ACTION_TYPES } from '../../constants/constants';

const user = (state = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES.UPDATE_USER:
      return action.user;
    default:
      return state;
  }
};

export default user;
