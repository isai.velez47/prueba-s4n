import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { TextField } from '@material-ui/core';
import '../scss/form.scss';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import DateFnsUtils from '@date-io/date-fns';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { connect } from 'react-redux';
import { updateUser } from '../redux/actions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
    },
  })
);

export interface GithubForm {
  dispatch: any;
}

const GithubForm: React.FC<GithubForm> = ({ dispatch }) => {
  const classes = useStyles();
  const { control, handleSubmit, formState } = useForm({
    mode: 'onChange',
  });
  const onSubmit = data => dispatch(updateUser(data));

  const [selectedDate, setSelectedDate] = React.useState<Date | null>(null);

  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date);
  };

  return (
    <section className="Form__container">
      <form onSubmit={handleSubmit(onSubmit)} className="Github_form">
        <Controller
          as={TextField}
          name="name"
          control={control}
          defaultValue=""
          required
          rules={{
            required: true,
            pattern: /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/,
          }}
          variant="outlined"
          label="Nombre completo"
        />
        <Controller
          as={TextField}
          name="identification"
          control={control}
          defaultValue=""
          required
          rules={{
            required: true,
            pattern: /^[0-9]*$/,
          }}
          variant="outlined"
          label="Cédula"
        />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            margin="normal"
            name="born_date"
            id="date-picker-dialog"
            label="Fecha de nacimiento"
            format="MM/dd/yyyy"
            required
            value={selectedDate}
            onChange={handleDateChange}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        </MuiPickersUtilsProvider>

        <Controller
          as={TextField}
          name="email"
          control={control}
          defaultValue=""
          variant="outlined"
          required
          rules={{
            required: true,
            pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
          }}
          label="Correo electrónico"
        />
        <Controller
          as={TextField}
          name="github_user"
          control={control}
          defaultValue=""
          required
          rules={{
            required: true,
          }}
          variant="outlined"
          label="Usuario de Github"
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.button}
          disabled={!formState.isValid}
          endIcon={
            <FontAwesomeIcon className="mr-3" size="lg" icon={faCheckCircle} />
          }
        >
          Guardar
        </Button>
      </form>
    </section>
  );
};

export default connect()(GithubForm);
