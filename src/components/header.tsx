import React, { useEffect, useState } from 'react';

import styled from '@emotion/styled';
import { css } from '@emotion/core';
import { Link } from 'gatsby';
import '../scss/header.scss';
import { IUser } from '../models/User';
import { connect } from 'react-redux';

const HeaderWrapper = styled.header`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  z-index: 100;
  background-color: ${({ hasBackground }: any) =>
    hasBackground ? 'rgb(104, 158, 205)' : 'transparent'};
  box-shadow: ${({ hasBackground }: any) =>
    hasBackground ? '0 0 10px rgba(0, 0, 0, 0.3)' : 'none'};
  padding: 0.5em 1em;
  transition: background-color 0.3s ease-in-out, box-shadow 0.4s ease;

  @media (max-width: 767px) {
    background-color: rgb(104, 158, 205);
  }
`;

const HeaderContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 50;
`;

const HeaderLogo = styled.img`
  height: 50px;

  @media (min-width: 992px) {
    height: 70px;
  }
`;

const NavBar = styled.nav`
  @media (min-width: 768px) {
    flex-direction: row;
    position: relative;
    background-color: transparent;
    top: 0;
    height: auto;
    width: auto;
    max-width: unset;
    padding-top: auto;
    right: auto;
    z-index: auto;
    box-shadow: none;
    padding: 0;
  }
`;

export interface HeaderProps {
  transparentOnTop?: boolean;
  user: IUser;
}

const Header: React.FC<HeaderProps> = ({ transparentOnTop, user }) => {
  const [isOnTop, setIsOnTop] = useState(true);
  const [showMenu, setShowMenu] = useState(false);

  const scrollHandler = () => {
    const { scrollTop } = document.documentElement;

    setIsOnTop(scrollTop === 0);
  };

  const toggleMenu = () => setShowMenu(!showMenu);

  useEffect(() => {
    // Initial call to verify position on the screen
    scrollHandler();

    window.addEventListener('scroll', scrollHandler);

    return () => window.removeEventListener('scroll', scrollHandler);
  }, []);

  return (
    <HeaderWrapper hasBackground={!isOnTop || !transparentOnTop}>
      <HeaderContent className="container">
        <Link to="/">
          <HeaderLogo
            src="/images/github-logo.png"
            className="mb-0"
            alt="GitHub logo"
          />
        </Link>
        <NavBar>
          <h2
            className="text-primary header-link"
            css={theme => ({
              textDecoration: 'none',
              padding: '.5rem 2rem',
              fontWeight: 100,
              position: 'relative',
              transition: 'all 150ms ease',
              borderRadius: '0.5em',
              margin: 0,

              color: 'black',
              ':hover': {
                color: 'white',
                backgroundColor: '#0A286A',
              },
            })}
            onClick={toggleMenu}
          >
            Usuario consultado:{' '}
            <strong>
              {!!user.github_user ? user.github_user : 'Sin usuario'}
            </strong>
          </h2>
        </NavBar>
      </HeaderContent>
    </HeaderWrapper>
  );
};

Header.defaultProps = {
  transparentOnTop: true,
};

export default connect(state => ({ user: state.user }))(Header);
