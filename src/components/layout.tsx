import React from 'react';
import Header from './header';

export interface LayoutProps {}

const Layout: React.FC<LayoutProps> = ({ children }) => (
  <div>
    <Header />
    <div className="hero-min">{children}</div>
  </div>
);

export default Layout;
