import React, { useEffect } from 'react';
import clsx from 'clsx';
import {
  createStyles,
  lighten,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { useForm, Controller } from 'react-hook-form';
import { TextField } from '@material-ui/core';

import { IUser } from '../models/User';
import { connect } from 'react-redux';
import '../scss/table.scss';
import { MAX_COUNT_WITH_FILTER } from '../constants/constants';

interface Data {
  language: string;
  default_branch: string;
  html_url: string;
  name: string;
  description: string;
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  const valueB = String(b[orderBy]).toLowerCase();
  const valueA = String(a[orderBy]).toLowerCase();
  if (valueB < valueA || valueB === 'null') {
    return -1;
  }
  if (valueB > valueA || valueA === 'null') {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  {
    id: 'language',
    numeric: false,
    disablePadding: true,
    label: 'Lenguaje',
  },
  {
    id: 'default_branch',
    numeric: false,
    disablePadding: false,
    label: 'Branch',
  },
  { id: 'html_url', numeric: false, disablePadding: false, label: 'Url' },
  { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
  {
    id: 'description',
    numeric: false,
    disablePadding: false,
    label: 'Descripción',
  },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => void;
  order: Order;
  orderBy: string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>
  ) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'default' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
    },
  })
);

interface EnhancedTableToolbarProps {
  user: IUser;
  onFilterListener: any;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles();
  const { user, onFilterListener } = props;

  const { control, handleSubmit, formState, watch } = useForm({
    mode: 'onChange',
  });

  const onSubmit = data => console.log(data);

  const filteredName = watch('name');
  useEffect(() => onFilterListener(filteredName), [filteredName]);

  return (
    <Toolbar className={clsx(classes.root)}>
      <Typography
        className={classes.title}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        Repositorios del usuario:{' '}
        <strong>{!!user.github_user ? user.github_user : 'Sin usuario'}</strong>
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)} className="Filter_form">
        <Controller
          as={TextField}
          name="name"
          control={control}
          defaultValue=""
          variant="outlined"
          label="Buscar por nombre"
        />
      </form>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  })
);

interface GithubTableProps {
  user: IUser;
}

const GithubTable: React.FC<GithubTableProps> = ({ user }) => {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('language');
  const [repos, setRepos] = React.useState<Data[]>([]);
  const [page, setPage] = React.useState(0);
  const [filteredName, setFilter] = React.useState<string>('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  useEffect(() => {
    if (!!user.github_user) {
      fetch(`https://api.github.com/users/${user.github_user}/repos`)
        .then(res => res.json())
        .then(
          result => setRepos(result),
          error => console.log(error)
        );
    }
  }, [user]);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) =>
    setPage(newPage);

  const handleSetFilter = (newFilter: string) => {
    if (!newFilter || newFilter.length < 3) {
      setFilter('');
      return;
    }
    setFilter(newFilter);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, repos.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar user={user} onFilterListener={handleSetFilter} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size="medium"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {stableSort(repos, getComparator(order, orderBy))
                .filter(repo => String(repo.name).indexOf(filteredName) !== -1)
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  // Si no hay filtros en la busqueda pinta toda la tabla
                  // Si hay filtro, hay que validar que la cantidad de repositorios mostrados sea menor a la cantidad máxima permitida (page * rowsPerPage + (index + 1))
                  return !filteredName ||
                    page * rowsPerPage + (index + 1) <=
                      MAX_COUNT_WITH_FILTER ? (
                    <TableRow hover role="checkbox" tabIndex={-1} key={labelId}>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.language}
                      </TableCell>
                      <TableCell align="left">{row.default_branch}</TableCell>
                      <TableCell align="left">{row.html_url}</TableCell>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">{row.description}</TableCell>
                    </TableRow>
                  ) : (
                    <></>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5]}
          component="div"
          count={
            !filteredName
              ? // Si no tiene filtros, muestra la cantidad total de repositorios
                repos.length
              : repos.filter(
                  repo => String(repo.name).indexOf(filteredName) !== -1
                ).length < MAX_COUNT_WITH_FILTER
              ? // Si tiene filtros y su cantidad de repositorios es mayor a la máxima permitida, se muestra la cantidad máxima permitida
                repos.filter(
                  repo => String(repo.name).indexOf(filteredName) !== -1
                ).length
              : MAX_COUNT_WITH_FILTER
          }
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <p>Filtro aplicado: {!!filteredName ? filteredName : 'Sin filtro'}</p>
    </div>
  );
};

export default connect(state => ({ user: state.user }))(GithubTable);
