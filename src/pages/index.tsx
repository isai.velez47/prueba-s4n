import React from 'react';
import 'react-awesome-slider/dist/styles.css';
import Fade from 'react-reveal/Fade';
import './../scss/index.scss';
import Layout from '../components/layout';
import GithubForm from '../components/form';
import GithubTable from '../components/table';

export interface IndexProps {}

const Index: React.FC<IndexProps> = () => (
  <div className="Index">
    <Layout>
      <section className="General__container">
        <div className="General__container-form">
          <Fade left>
            <GithubForm />
          </Fade>
        </div>
        <div className="General__container-table">
          <Fade right>
            <GithubTable />
          </Fade>
        </div>
      </section>
    </Layout>
  </div>
);

export default Index;
