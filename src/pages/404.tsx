import React from 'react';

export interface NotFoundProps {}

const NotFound: React.FC<NotFoundProps> = () => (
  <div>
    <h3>Page not found</h3>
  </div>
);

export default NotFound;
