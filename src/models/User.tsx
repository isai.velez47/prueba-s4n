export interface IUser {
  name: string;
  identification: string;
  email: string;
  github_user: string;
  born_date: Date;
}
